﻿using System;
using System.Configuration;
using System.ServiceModel;
using System.ServiceModel.Channels;

namespace DesignPattern.WCFClient
{
    /// <summary>
    /// Proxy Service
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <seealso cref="System.IDisposable" />
    public class ProxyService<T> : IDisposable where T : class
    {
        #region Constants

        /// <summary>
        /// The WCF serializer maximum string content length
        /// </summary>
        private const int WcfSerializerMaxStringContentLength = (16*1024*1024); //16MB;
        /// <summary>
        /// The WCF serializer maximum array length
        /// </summary>
        private const int WcfSerializerMaxArrayLength = 65535; //16bit
        /// <summary>
        /// The WCF serializer maximum bytes per read
        /// </summary>
        private const int WcfSerializerMaxBytesPerRead = (16*1024*1024); //16MB
        /// <summary>
        /// The WCF serializer maximum depth
        /// </summary>
        private const int WcfSerializerMaxDepth = 65535; //16bit;
        //private const int WcfSerializerMaxNameTableCharCount = 16384; //default 16K

        #endregion

        #region Private Variables

        /// <summary>
        /// The _sync
        /// </summary>
        private readonly object _sync = new object();
        /// <summary>
        /// The _channel factory
        /// </summary>
        private IChannelFactory<T> _channelFactory;
        /// <summary>
        /// The _channel
        /// </summary>
        private T _channel;
        /// <summary>
        /// The _disposed
        /// </summary>
        private bool _disposed;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ProxyService{T}"/> class.
        /// </summary>
        public ProxyService() : this(string.Empty)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProxyService{T}"/> class.
        /// </summary>
        /// <param name="endpoint">The endpoint.</param>
        public ProxyService(string endpoint) : this(endpoint, 0)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProxyService{T}"/> class.
        /// </summary>
        /// <param name="closeTimeout">The close timeout.</param>
        public ProxyService(int closeTimeout) : this (string.Empty, closeTimeout) 
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProxyService{T}"/> class.
        /// </summary>
        /// <param name="endpoint">The endpoint.</param>
        /// <param name="closeTimeout">The close timeout.</param>
        public ProxyService(string endpoint, int closeTimeout)
        {
            ServiceEndPoint = endpoint;
            CloseTimeout = closeTimeout <= 1 ? 1 : closeTimeout;
            Initialise();
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the service end point.
        /// </summary>
        /// <value>
        /// The service end point.
        /// </value>
        public string ServiceEndPoint { get; set; }

        /// <summary>
        /// Gets or sets the close timeout.
        /// </summary>
        /// <value>
        /// The close timeout.
        /// </value>
        public int CloseTimeout { get; set; }

        /// <summary>
        /// Gets the channel.
        /// </summary>
        /// <value>
        /// The channel.
        /// </value>
        public T Channel
        {
            get
            {
                Initialise();
                return _channel;
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Invokes the service.
        /// </summary>
        /// <typeparam name="TR">The type of the r.</typeparam>
        /// <param name="invokeHandler">The invoke handler.</param>
        /// <returns></returns>
        public object InvokeService<TR>(Func<T, TR> invokeHandler)
        {
            var arg = Channel;
            var obj2 = (ICommunicationObject)arg;
            try
            {
                return invokeHandler(arg);
            }
            finally
            {
                try
                {
                    if (obj2.State != CommunicationState.Faulted)
                    {
                        obj2.Close();
                    }
                }
                catch
                {
                    obj2.Abort();
                }
            }
        }

        /// <summary>
        /// Closes the channel.
        /// </summary>
        public void CloseChannel()
        {
            if (_channel == null) return;
            try
            {
                ((ICommunicationObject)_channel).Close();
            }
            catch
            {
                ((ICommunicationObject)_channel).Abort();
            }
        }

        /// <summary>
        /// Closes the factory.
        /// </summary>
        public void CloseFactory()
        {
            if (_channelFactory == null) return;
            try
            {
                _channelFactory.Close();
            }
            catch
            {
                _channelFactory.Abort();
            }
        }

        #endregion

        #region Private Methods 

        /// <summary>
        /// Initialises this instance.
        /// </summary>
        private void Initialise()
        {
            lock (_sync)
            {
                if (_channel != null) return;
                _channelFactory = GetChannelFactory();
                _channel = _channelFactory.CreateChannel(new EndpointAddress(ServiceEndPoint));
                ((IClientChannel) _channel).Faulted += ChannelFaulted;
            }
        }

        /// <summary>
        /// Gets the channel factory.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="ApplicationException"></exception>
        private ChannelFactory<T> GetChannelFactory()
        {   
            var serviceName = (typeof (T)).FullName;
            var serviceEndpointUrl = ServiceEndPoint;

            if (string.IsNullOrEmpty(serviceEndpointUrl))
            {
                var configSetting = string.Format("SERVICE_URL:{0}", serviceName);
                serviceEndpointUrl = ConfigurationManager.AppSettings[configSetting];
            }

            ChannelFactory<T> channelFactory = null;

            if (string.IsNullOrWhiteSpace(serviceEndpointUrl))
            {
                //no appsetting, throw exception.
                throw new ApplicationException(string.Format("Missing Application Setting for '{0}'", serviceName));
            }

            Binding binding = null;

            // http binding
            if (serviceEndpointUrl.StartsWith("http://"))
            {
                binding = new BasicHttpBinding
                {
                    ReaderQuotas =
                    {
                        MaxStringContentLength = WcfSerializerMaxStringContentLength,
                        MaxArrayLength = WcfSerializerMaxArrayLength,
                        MaxBytesPerRead = WcfSerializerMaxBytesPerRead,
                        MaxDepth = WcfSerializerMaxDepth
                    },
                    MaxBufferPoolSize = int.MaxValue,
                    MaxBufferSize = int.MaxValue,
                    MaxReceivedMessageSize = int.MaxValue,
                    CloseTimeout = TimeSpan.FromMinutes(CloseTimeout),
                    OpenTimeout = TimeSpan.FromMinutes(CloseTimeout),
                    ReceiveTimeout = TimeSpan.FromMinutes(CloseTimeout),
                    SendTimeout = TimeSpan.FromMinutes(CloseTimeout)
                };
            }

            // http binding
            else if (serviceEndpointUrl.StartsWith("https://"))
            {
                binding = new BasicHttpsBinding
                {
                    ReaderQuotas =
                    {
                        MaxStringContentLength = WcfSerializerMaxStringContentLength,
                        MaxArrayLength = WcfSerializerMaxArrayLength,
                        MaxBytesPerRead = WcfSerializerMaxBytesPerRead,
                        MaxDepth = WcfSerializerMaxDepth
                    },
                    MaxBufferPoolSize = int.MaxValue,
                    MaxBufferSize = int.MaxValue,
                    MaxReceivedMessageSize = int.MaxValue,
                    CloseTimeout = TimeSpan.FromMinutes(CloseTimeout)
                };
            }

            // http binding
            else if (serviceEndpointUrl.StartsWith("net.tcp://"))
            {
                binding = new NetTcpBinding
                {
                    ReaderQuotas =
                    {
                        MaxStringContentLength = WcfSerializerMaxStringContentLength,
                        MaxArrayLength = WcfSerializerMaxArrayLength,
                        MaxBytesPerRead = WcfSerializerMaxBytesPerRead,
                        MaxDepth = WcfSerializerMaxDepth
                    },
                    MaxBufferPoolSize = int.MaxValue,
                    MaxBufferSize = int.MaxValue,
                    MaxReceivedMessageSize = int.MaxValue,
                    CloseTimeout = TimeSpan.FromMinutes(CloseTimeout)
                };
            }

            // http binding
            else if (serviceEndpointUrl.StartsWith("net.pipe://"))
            {
                binding = new NetNamedPipeBinding
                {
                    ReaderQuotas =
                    {
                        MaxStringContentLength = WcfSerializerMaxStringContentLength,
                        MaxArrayLength = WcfSerializerMaxArrayLength,
                        MaxBytesPerRead = WcfSerializerMaxBytesPerRead,
                        MaxDepth = WcfSerializerMaxDepth
                    },
                    MaxBufferPoolSize = int.MaxValue,
                    MaxBufferSize = int.MaxValue,
                    MaxReceivedMessageSize = int.MaxValue,
                    CloseTimeout = TimeSpan.FromMinutes(CloseTimeout)
                };

            }

            if (binding != null) channelFactory = new ChannelFactory<T>(binding);

            if (channelFactory != null)
            {
                channelFactory.Faulted += FactoryFaulted;
                channelFactory.Open();
            }

            ServiceEndPoint = serviceEndpointUrl;

            return channelFactory;
        }

        /// <summary>
        /// Channels the faulted.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private static void ChannelFaulted(object sender, EventArgs e)
        {
            var channel = (IClientChannel) sender;
            try
            {
                channel.Close();
            }
            catch
            {
                channel.Abort();
            }
        }

        /// <summary>
        /// Factories the faulted.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        private static void FactoryFaulted(object sender, EventArgs args)
        {
            var factory = (ChannelFactory) sender;
            try
            {
                factory.Close();
            }
            catch
            {
                factory.Abort();
            }
        }

        #endregion

        #region DeConstructors

        /// <summary>
        /// Finalizes an instance of the <see cref="ProxyService{T}"/> class.
        /// </summary>
        ~ProxyService()
        {
            Dispose(false);
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposed"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        public void Dispose(bool disposed)
        {
            if (_disposed) return;

            if (disposed)
            {
                lock (_sync)
                {
                    CloseChannel();
                    CloseFactory();

                    if (_channelFactory != null)
                    {
                        ((IDisposable)_channelFactory).Dispose();
                    }

                    _channel = null;
                    _channelFactory = null;
                }
            }

            _disposed = true;
        }
        #endregion
    }
}
